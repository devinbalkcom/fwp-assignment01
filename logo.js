var sketchProc=function(processingInstance){ with (processingInstance){
size(200, 200);  // create a Processing canvas 200 pixels by 200 pixels

/* Your code goes below this comment */
background(255, 255, 255);  // set background to white

fill(0,0,0);
ellipse(100,100,150,150);

fill(0,0,200);
ellipse(100,100,100,100);

strokeWeight(10);
stroke(255);
line( 100, 0, 100, 200 );

strokeWeight(10);
stroke(255);
line( 100, 0, 100, 200 );

strokeWeight(10);
stroke(255);
line( 0, 100, 200, 100 );

/* End of your code */

}};
